package com.atlassian.pdkinstall;

import com.atlassian.plugin.DefaultPluginArtifactFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import org.osgi.framework.BundleContext;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Installs the plugin into system.  Supports plugin jar and obr files.
 */
public class PluginInstaller
{
    private final PluginController pluginController;
    private final PluginAccessor pluginAccessor;
    private final DefaultPluginArtifactFactory pluginArtifactFactory;
    private final BundleContext bundleContext;

    public PluginInstaller(final PluginController pluginController, final PluginAccessor pluginAccessor, final BundleContext bundleContext)
    {
        this.pluginController = pluginController;
        this.pluginAccessor = pluginAccessor;
        this.bundleContext = bundleContext;
        pluginArtifactFactory = new DefaultPluginArtifactFactory();
    }

    public synchronized List<String> install(File plugin) throws Exception
    {
        List<String> errors = new ArrayList<String>();
        if (plugin.getName().endsWith(".obr"))
        {
            // Prevents loading of the class that needs the RepositoryAdmin from OBR, which may not be present
            new ObrPluginTypeInstaller(pluginController, bundleContext).install(plugin, errors);
        }
        else
        {
            String key = pluginController.installPlugin(pluginArtifactFactory.create(plugin.toURI()));
            if (!pluginAccessor.isPluginEnabled(key))
            {
                pluginController.enablePlugin(key);
            }
        }

        return errors;
    }
}
